FROM node:17.2.0-alpine as builder

WORKDIR /home/node/app

COPY package*.json ./

RUN npm i

COPY . .

FROM builder as compiler

ENV NODE_PATH=./build

RUN npx tsc

FROM builder as runner

ENTRYPOINT ["node", "built/main.js"]
