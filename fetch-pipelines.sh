#!/bin/bash

set -e
ref=${1:-}
sha=${2:-}

project_id=29396387

        # | jq 'map(select(.status=="success"))' \
pipelines=$(curl -s "https://gitlab.com/api/v4/projects/nomadic-labs%2Farvid-tezos/pipelines/?project_id=${project_id}&ref=${ref}&sha=${sha}" \
            | jq 'map(.id)[]')

# gitlab -d -o json project-pipeline list --project-id $project_id --ref "$ref" | jq 'map(select(.status=="success"))|map(.id)[]'

# echo $pipelines

folder=pipeline-"$ref"-"$sha";
mkdir -p "$folder"

for id in $pipelines; do
    echo -n "Fetching pipeline $id... "
    out_name=$folder/pipeline-"$ref"-"$sha"-"$id".json
    gitlab -o json project-pipeline-job list --all --project-id $project_id --pipeline-id "$id" > "$out_name"
    echo "Wrote $out_name"
done


