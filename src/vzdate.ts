
export function date_str_to_epoch(s: string): number {
    return new Date(s).getTime() / 1000
}

export function epoch_to_date_str(t: number): string {
    let d = new Date();
    d.setTime(t * 1000);
    return d.toISOString()
}
