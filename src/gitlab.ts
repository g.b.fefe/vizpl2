import { Context } from "./context.js";
import * as Pipelines from "./pipelines.js";
import { Option, fold, none, some } from "./option.js";

export type project_id = {
    ns: string,
    project: string
} | number

function project_id_tostring(p: project_id): string {
    if (p['ns']) {
        return p['ns'] + '/' + p['project'];
    } else {
        return '' + p;
    }
}

function project_id_gl(p: project_id): string | number {
    if (typeof (p) === 'object') {
        return p['ns'] + '/' + p['project'];
    } else if (typeof (p) === 'number') {
        return p;
    }
}

export type pipeline_ref = {
    id: number,
    project_id: project_id
}


export type branch_ref = {
    name: string,
    project_id: project_id
}

export type merge_request_ref = {
    id: number,
    project_id: project_id
}

function parse_ref(
    ref: string,
    obj_delimiter: string,
    default_namespace: string,
    default_project: string
): [project_id, string, string] {
    function mk_project_id(project = default_project, ns = default_namespace) {
        return {
            'ns': ns,
            'project': project
        };
    }

    let re_full = new RegExp(
        /^(?<ns>[\w0-9_\-.]+)\/(?<project>[\w0-9_\-.]+)/.source
        + obj_delimiter
        + /(?<reference>\w+)/.source
    )
    let m = ref.match(re_full);
    if (m) {
        return [mk_project_id(m.groups['project'], m.groups['ns']), m.groups['reference'], ref.substr(m[0].length)];
    }

    let re_project = new RegExp(
        /^(?<project>[\w0-9_\-.]+)/.source
        + obj_delimiter
        + /(?<reference>\w+)/.source
    );
    m = ref.match(re_project);
    if (m) {
        return [mk_project_id(m.groups['project']), m.groups['reference'], ref.substr(m[0].length)];
    }


    let re_short = new RegExp(
        /^/.source
        + obj_delimiter
        + /(?<reference>\w+)/.source
    );
    m = ref.match(re_short);
    if (m) {
        return [mk_project_id(), m.groups['reference'], ref.substr(m[0].length)];
    }

    return null;
}

export function parse_pipeline_ref(
    pipeline_ref: string,
    default_namespace: string,
    default_project: string)
    : [pipeline_ref, string] {

    if (pipeline_ref.match(/^\d+$/)) {
        return [{
            'id': parseInt(pipeline_ref, 10),
            'project_id': {
                'ns': default_namespace,
                'project': default_project,
            }
        }, ''];
    }

    let r = parse_ref(pipeline_ref, '#', default_namespace, default_project);
    if (r) {
        let [project_id, reference, ref_rest] = r;
        return [{
            'id': parseInt(reference, 10),
            'project_id': project_id,
        }, ref_rest];
    } else {
        return null;
    }
}


export function parse_merge_request_ref(
    merge_request_ref: string,
    default_namespace: string,
    default_project: string)
    : [merge_request_ref, string] {
    let r = parse_ref(
        merge_request_ref, '!', default_namespace, default_project
    );
    if (r) {
        let [project_id, reference, ref_rest] = r;
        return [{
            'id': parseInt(reference, 10),
            'project_id': project_id,
        }, ref_rest];
    } else {
        return null;
    }
}


export function parse_branch_ref(
    branch_ref: string,
    default_namespace: string,
    default_project: string)
    : [branch_ref, string] {
    let r = parse_ref(
        branch_ref, '/', default_namespace, default_project
    );
    if (r) {
        let [project_id, reference, ref_rest] = r;
        return [{
            'name': reference,
            'project_id': project_id,
        }, ref_rest];
    } else {
        return null;
    }
}

export async function project_pipelines_jobs(
    ctxt: Context,
    pipeline_ref: pipeline_ref
): Promise<Pipelines.raw_pipeline> {
    let per_page = 200;
    let project_id_s = encodeURIComponent(project_id_tostring(pipeline_ref.project_id));
    let url = "https://gitlab.com/api/v4/projects/"
        + project_id_s
        + "/pipelines/"
        + pipeline_ref.id
        + "/jobs?per_page="
        + per_page;

    return ctxt.read_remote_json(url).then(
        (json: Pipelines.raw_pipeline) => json
    );
}

type gitlab_pipeline = {
    'id': number,
    'iid': number,
    'status': string
}

export async function project_branch_pipelines_jobs(
    ctxt: Context,
    branch_ref: branch_ref,
    sha: Option<string>,
    limit: Option<number>
): Promise<Pipelines.raw_pipeline[]> {
    let project_id_s = encodeURIComponent(project_id_tostring(branch_ref.project_id));
    let url = "https://gitlab.com/api/v4/projects/"
        + project_id_s
        + "/pipelines?"
        + "ref=" + branch_ref.name
        + fold(sha, (v => "&sha=" + v), '')
        + fold(limit, (v => "&per_page=" + v), '');

    return ctxt.read_remote_json(url).then(
        (json: gitlab_pipeline[]) => {
            return Promise.all(json.map((json: gitlab_pipeline) => {
                let pipeline: pipeline_ref = {
                    'id': json.id,
                    'project_id': branch_ref.project_id
                };
                return project_pipelines_jobs(ctxt, pipeline);
            }));
        }
    );

}

export async function resolve_branch_ref(
    _ctxt: Context,
    _branch_ref: branch_ref
): Promise<Pipelines.raw_pipeline[]> {
    // Project.
    return Promise.resolve([]);
}
