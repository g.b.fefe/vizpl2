import { Context } from "./context.js";
import * as Pipelines from "./pipelines.js";
import * as Gitlab from "./gitlab.js";
// import { MergedJob } from "./pipelines.js";
import { output_type } from "./output.js";

import { ADT, matchI } from "ts-adt";

import { Option, some, none, is_none, is_some, of_some } from "./option.js";

import * as formatting from "./formatting.js";


export interface Config {
    formatter: formatting.formatter;
    outputs: Set<output_type>;
    job_filter: Option<RegExp>;
}

export interface Arguments {
    config: Config,
    datasets: Pipelines.Dataset[];
}


type dataset_filters = {
    sha: Option<string>,
    limit: Option<number>,
    jq_filter: Option<string>
    // jmes_filter: Option<string>
}

export let no_filters: dataset_filters = {
    sha: none(),
    limit: none(),
    jq_filter: none()
};

export type dataset_ref = ADT<{
    local: { path: string };
    pipeline: {
        pipeline_ref: Gitlab.pipeline_ref
    };
    branch: {
        branch_ref: Gitlab.branch_ref,
        filters: dataset_filters,
    };
    merge_request: {
        merge_request_ref: Gitlab.merge_request_ref,
        filters: dataset_filters
    };
}>;

function parse_dataset_filters(input: string): [dataset_filters, string] {
    let s = input;
    let filters = no_filters;
    let sha_match: RegExpMatchArray;
    if (sha_match = s.match(/^@(\w+)/)) {
        filters.sha = some(sha_match[1]);
        s = s.substr(sha_match[0].length);
    }

    let limit_match: RegExpMatchArray;
    if (limit_match = s.match(/^~(\d+)/)) {
        filters.limit = some(parseInt(limit_match[1], 10));
        s = s.substr(limit_match[0].length);
    }

    let filter_match: RegExpMatchArray;
    if (filter_match = s.match(/^\|(\w+)$/)) {
        filters.jq_filter = some(filter_match[1]);
        s = s.substr(filter_match[0].length);
    }

    return [filters, s];
}

export function parse_dataset_reference(input: string, def_ns: string, def_proj: string): Option<dataset_ref> {
    let pipeline_ref: Gitlab.pipeline_ref;
    let branch_ref: Gitlab.branch_ref;
    let merge_request_ref: Gitlab.merge_request_ref;
    let s = input;

    if (s.endsWith('.json')) {
        return some({ _type: 'local', path: s });
    } else {
        let ref: dataset_ref;
        let filters: dataset_filters;
        let r: any;
        if (r = Gitlab.parse_pipeline_ref(s, def_ns, def_proj)) {
            [pipeline_ref, s] = r;
            ref = { _type: 'pipeline', pipeline_ref };
        } else if (r = Gitlab.parse_merge_request_ref(s, def_ns, def_proj)) {
            [merge_request_ref, s] = r;
            [filters, s] = parse_dataset_filters(s);
            ref = { _type: 'merge_request', merge_request_ref, filters };
        } else if (r = Gitlab.parse_branch_ref(s, def_ns, def_proj)) {
            [branch_ref, s] = r;
            [filters, s] = parse_dataset_filters(s);
            ref = { _type: 'branch', branch_ref, filters };
        } else {
            console.log("not sure what to do with", s);
            return none();
        }

        if (s.length != 0) {
            console.log("invalid ref " + input + ", trailing input: " + s);
            return none()
        } else {
            return some(ref);
        }
    }
}

/*

  references:
  - pipeline.json = a local file containing the results of a get jobs api call
  - will fetch the local file if it exists
  - ((project)/repo)#pipeline_id (e.g. tezos/tezos#386210384)
  - will fetch the pipeline jobs from the api

  todo:

  - ((namespace)/project)/branch(@commit)
  - fetch pipelines that have run on this branch (and commit)

  - ((namespace)/project)!mr(@commit)
  - fetch merge request pipelines that have run on this mr (and commit)

  - above~N
  - fetch N latest pipelines of branch / MR

  - above|jq-expression
  - above||jmes-expression
  - as above but apply jq expression to filter results

*/

function resolve_dataset_reference(
    ctxt: Context,
    reference: string
): Promise<Pipelines.raw_pipeline[]> {
    // cast object to Pipelines.raw_pipeline
    let read_local_pipeline: (ref: string) => Promise<Pipelines.raw_pipeline>
        = (ref) => ctxt.read_local_json(ref).then((obj: Pipelines.raw_pipeline) => obj)

    // let pipeline_ref_opt;
    let pipeline_ref_opt = parse_dataset_reference(reference, 'tezos', 'tezos')

    // [pipeline_ref_opt, _m] = Gitlab.parse_pipeline_ref(reference, 'tezos', 'tezos');

    if (is_none(pipeline_ref_opt)) {
        ctxt.error("Could not parse pipeline reference: " + reference)
    } else {
        let pipeline_ref = of_some(pipeline_ref_opt);

        function singleton<T>(x: T): T[] { return [x]; };

        return matchI(pipeline_ref)({
            'local': async ({ path }) => {
                // console.log(reference + ": fetch local path " + path);
                return read_local_pipeline(path)
                    .then(singleton);
            },
            'pipeline': async ({ pipeline_ref }) => {
                // console.log(reference + ": fetch pipeline", pipeline_ref, filters);
                return Gitlab
                    .project_pipelines_jobs(ctxt, pipeline_ref)
                    .then(singleton);
            },
            'branch': ({ branch_ref, filters }) => {
                return Gitlab
                    .project_branch_pipelines_jobs(ctxt, branch_ref, filters.sha, filters.limit);
            },
            'merge_request': ({ merge_request_ref, filters }) => {
                console.log(reference + ": mr (TODO)", merge_request_ref, filters);
                return Promise.resolve([]);
            }
        });

    }

    // return [];


    // let _m;
    // let pipeline_ref : Gitlab.pipeline_ref;
    // if (reference.endsWith('.json')) {
    //     return [read_local_pipeline(reference)];
    // } else if ([pipeline_ref, _m] = Gitlab.parse_pipeline_ref(reference, 'tezos', 'tezos')) {
    //     return [
    //         Gitlab.project_pipelines_jobs(ctxt, pipeline_ref)
    //     ];
    // }


}

async function pmap<T1, T2>(p: Promise<T1>, f: (v: T1) => T2): Promise<T2> {
    // return p.then((v : T1) => f(v))
    return p.then(f)
}

export async function parse_command_line(
    ctxt: Context,
    args: string[],
): Promise<Arguments> {
    if (args.length == 0) {
        ctxt.error("Must provide data sets as args");
    }

    // parse output configuration
    let formatter: formatting.formatter =
        (secs, b) => formatting.d2_formatter(secs, b);

    let outputs: Set<output_type> = new Set();

    let job_filter: Option<RegExp> = none();

    while (args.length) {
        if (args[0] == "--humanize") {
            formatter = (secs, b) => formatting.humanize_formatter(secs, b);
            args.shift();
        } else if (args[0] == "--table") {
            outputs.add(output_type.Table);
            args.shift();
        } else if (args[0] == "--graph") {
            outputs.add(output_type.Graph);
            args.shift();
        } else if (args[0] == "--job-filter") {
            args.shift();
            if (args.length == 0) {
                ctxt.error("--job-filter takes exactly one argument");
            }
            if (is_some(job_filter)) {
                ctxt.error("--job-filter can only be given once");
            }
            job_filter = some(new RegExp(args[0]));
            args.shift();
        } else {
            break;
        }
    }


    outputs = outputs.size ? outputs : new Set([output_type.Table]);

    // parse data set references
    let datasets: Promise<Pipelines.Dataset>[] = [];
    if (args.indexOf("--label") >= 0) {
        let pipelines: Promise<Pipelines.pipeline[]>[] = [];

        while (args.length > 0) {
            let arg = args.shift();

            if (arg == "--label") {
                if (args.length == 0) {
                    ctxt.error("Must provide label after --label")
                } else if (pipelines.length == 0) {
                    ctxt.error("No datasets provided before label")
                } else {
                    let label = args.shift();
                    let ds_ps =
                        Promise.all(pipelines).then((ps) => ({
                            label: label,
                            pipelines: ps.flat()
                        }));
                    datasets.push(ds_ps);
                    pipelines = [];
                }
            } else {
                let next = resolve_dataset_reference(ctxt, arg)
                    .then((ps) => ps.map(Pipelines.annotate_pipeline));
                // .map((p) => p.then(Pipelines.annotate_pipeline));
                pipelines.push(next);
            }
        }

        if (pipelines.length > 0) {
            let ds_p = Promise.all(pipelines).then((pipelines) => ({
                label: "Unnamed dataset",
                pipelines: pipelines.flat()
            }));
            datasets.push(ds_p);
        }
    } else {
        datasets = args.map((ref) =>
            resolve_dataset_reference(ctxt, ref).then(
                (raw_pipelines: Pipelines.raw_pipeline[]) => {
                    return Promise.resolve({
                        label: ref,
                        pipelines: raw_pipelines.map(Pipelines.annotate_pipeline)
                    })
                }
            )
        );
    }

    return Promise.all(datasets).then((datasets) =>
        Promise.resolve({
            config: {
                formatter: formatter,
                outputs: outputs,
                job_filter: job_filter
            },
            datasets: datasets
        })
    );
}
