import { context } from "./cli_context.js";

let error = context.error;

// Assertion functions
function check(value: boolean, msg: string = "assertion failed") {
    if (!value) { error("Assertion failed: " + msg); }
}

function check_eq<Type>(left: Type, right: Type, msg: string = '') {
    if (left !== right) {
        error("Assertion failed. Expected `" + left + "` to equal `" + right + "`. " + msg);
    }
}

function check_json_eq(left: object, right: object, msg: string) {
    let ljson = JSON.stringify(left);
    let rjson = JSON.stringify(right);
    check_eq(ljson, rjson, msg);
}

function check_is_nan(val: number, msg: string) {
    if (!isNaN(val)) {
        error("Assertion failed. Expected " + val + " to be NaN. " + msg)
    }
}

// Tests
import * as S from "./statistics.js";

check_eq(S.sum([]), 0, "sum([]) == 0")
check_is_nan(S.mean([]), "mean([]) == NaN")
check_is_nan(S.stddev([]), "stddev([]) == NaN")
check_is_nan(S.min([]), "min([]) == NaN")
check_eq(S.min([1, 2, 3, 4]), 1, "min([1,2,3,4])")
check_is_nan(S.max([]), "max([]) == NaN")
check_eq(S.max([1, 2, 3, 4]), 4, "max([1,2,3,4])")
check_is_nan(S.median([]), "max([]) == NaN")

// gitlab

import * as GL from "./gitlab.js"

console.log("test gitlab");

check_json_eq(
    GL.parse_pipeline_ref('ns/tezos#123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'ns', project: 'tezos' } },
    'ns/tezos#123123'
)

check_json_eq(
    GL.parse_pipeline_ref('nomadic-labs/arvid-tezos#123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'nomadic-labs', project: 'arvid-tezos' } },
    'nomadic-labs/arvid-tezos#123123'
)

check_json_eq(
    GL.parse_pipeline_ref('ns/tezos#123123@deadbeef', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'ns', project: 'tezos' } },
    'ns/tezos#123123'
)

check_json_eq(
    GL.parse_pipeline_ref('name.space-123/tezos#123123@deadbeef', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'name.space-123', project: 'tezos' } },
    'name.space-123/tezos#123123'
)

check_json_eq(
    GL.parse_pipeline_ref('4567', 'def_ns', 'def_proj')[0],
    { id: 4567, project_id: { ns: 'def_ns', project: 'def_proj' } },
    '4567'
)

check_json_eq(
    GL.parse_pipeline_ref('#123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'def_ns', project: 'def_proj' } },
    '#123123'
)
check_json_eq(
    GL.parse_pipeline_ref('tezos#123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'def_ns', project: 'tezos' } },
    'tezos#123123'
)
check_json_eq(
    GL.parse_pipeline_ref('foobar', 'def_ns', 'def_proj'),
    null,
    'foobar'
)


check_json_eq(
    GL.parse_merge_request_ref('ns/tezos!123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'ns', project: 'tezos' } },
    'ns/tezos!123123'
)

check_json_eq(
    GL.parse_merge_request_ref('!123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'def_ns', project: 'def_proj' } },
    '!123123'
)
check_json_eq(
    GL.parse_merge_request_ref('tezos!123123', 'def_ns', 'def_proj')[0],
    { id: 123123, project_id: { ns: 'def_ns', project: 'tezos' } },
    'tezos!123123'
)
check_json_eq(
    GL.parse_merge_request_ref('foobar', 'def_ns', 'def_proj'),
    null,
    'foobar'
)

check_json_eq(
    GL.parse_branch_ref('ns/tezos/mybranch', 'def_ns', 'def_proj')[0],
    { name: 'mybranch', project_id: { ns: 'ns', project: 'tezos' } },
    'ns/tezos/mybranch'
)

check_json_eq(
    GL.parse_branch_ref('/mybranch', 'def_ns', 'def_proj')[0],
    { name: 'mybranch', project_id: { ns: 'def_ns', project: 'def_proj' } },
    '/mybranch'
)
check_json_eq(
    GL.parse_branch_ref('tezos/mybranch', 'def_ns', 'def_proj')[0],
    { name: 'mybranch', project_id: { ns: 'def_ns', project: 'tezos' } },
    'tezos/mybranch'
)
check_json_eq(
    GL.parse_branch_ref('foobar', 'def_ns', 'def_proj'),
    null,
    'foobar'
)

// datafetcher

import * as D from "./datafetcher.js"
import * as O from "./option.js"

let values: { ref: string, exp: O.Option<D.dataset_ref> }[] = [
    {
        ref: 'ns/tezos#123123',
        exp: O.some({
            _type: 'pipeline',
            pipeline_ref: { id: 123123, project_id: { ns: 'ns', project: 'tezos' } }
        })
    },
    {
        ref: 'ns/tezos/mybranch@deadbeef',
        exp: O.some({
            _type: 'branch',
            branch_ref: { name: 'mybranch', project_id: { ns: 'ns', project: 'tezos' } },
            filters: {
                sha: O.some('deadbeef'),
                limit: O.none(),
                jq_filter: O.none()
            }
        })
    }
];

for (let v of values) {
    let ref: string = v['ref'];
    let expected: O.Option<D.dataset_ref> = v['exp'];

    // console.log(ref);
    // console.log('expected', expected);
    let got = D.parse_dataset_reference(ref, 'def_ns', 'def_proj');
    // console.log('got', got);
    check_json_eq(expected, got, ref);
}

console.log("Test graph");

import * as AsciiGraph from "./asciigraph.js"

let bar_tests: [number, number, number, string, number][] = [
    // resolution, start, length, expected bar, expected length
    [8, 0, 0, '', 0],
    [8, 0, 1, '▏', 1],
    [8, 0, 4, '▌', 1],
    [8, 0, 7, '▉', 1],
    [8, 0, 8, '█', 1],
    [8, 0, 16, '██', 2],
    [8, 8, 8, ' █', 2]
];
for (let [resolution, start, length, expected_bar, expected_length] of bar_tests) {
    let [bar, bar_length] = AsciiGraph.bar(resolution, start, length);
    check_eq(bar, expected_bar, `AsciiGraph.bar(${resolution}, ${start}, ${length})[bar]`);
    check_eq(bar_length, expected_length, `AsciiGraph.bar(${resolution}, ${start}, ${length})[length]`);
}

console.log("Done!");
