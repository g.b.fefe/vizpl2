// Is zero when vals is empty
export function sum(vals: number[]): number {
    return vals.reduce((val, acc) => acc + val, 0);
}

export function count<Type>(arr: Type[], fn: (el: Type) => boolean): number {
    return sum(arr.map((el) => fn(el) ? 1 : 0))
}

// Is NaN when vals is empty
export function mean(vals: number[]): number {
    return sum(vals) / vals.length;
}

// Is NaN when vals is empty
export function stddev(vals: number[]): number {
    let m = mean(vals);
    let variance = mean(vals.map((v) => Math.pow(m - v, 2)));
    return Math.sqrt(variance);
}

// Is NaN when vals is empty
export function min(vals: number[]): number {
    return vals.length
        ? vals.reduce((val, acc) => val < acc ? val : acc)
        : NaN;
}

// Is NaN when vals is empty
export function max(vals: number[]): number {
    return vals.length
        ? vals.reduce((val, acc) => val > acc ? val : acc)
        : NaN;
}

// Is NaN when vals is empty
export function median(vals: number[]): number {
    if (vals.length == 0) {
        return NaN;
    } else if (vals.length % 2 == 0) {
        let a = vals[vals.length / 2];  // 4 -> 2
        let b = vals[vals.length / 2 - 1]; // 4 -> 1 [0,1,2,3]
        return (a + b) / 2;
    } else {
        return vals[Math.floor(vals.length / 2)]; // 5 -> 2.5 -> 2 -[0,1,2,3,4]
    }
}

export interface Stats {
    mean: number;
    median: number;
    stddev: number;
    min: number;
    max: number;
}

export function stats(vals: number[]): Stats {
    return {
        mean: mean(vals),
        median: median(vals),
        stddev: stddev(vals),
        min: min(vals),
        max: max(vals),
    }
}
