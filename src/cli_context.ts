import * as fs from 'fs';
// import * as http from 'http';
import * as https from 'https';

import { Context } from "./context.js";

function error(msg: string): void {
    console.error(msg);
    process.exit(1);
}

async function read_local_json(file: string): Promise<object> {
    // console.log("reading file", file);
    let rawdata = fs.readFileSync(file);
    let json = JSON.parse(rawdata.toString());
    return Promise.resolve(json);
}

function http_request(params: string | URL, post_data: any): Promise<object> {
    return new Promise(function(resolve, reject) {
        var req = https.request(params, function(res) {
            // reject on bad status
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            // cumulate data
            var body = [];
            res.on('data', function(chunk) {
                body.push(chunk);
            });
            // resolve on end
            res.on('end', function() {
                try {
                    body = JSON.parse(Buffer.concat(body).toString());
                } catch (e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        // reject on request error
        req.on('error', function(err) {
            // This is not a "Second reject", just a different sort of failure
            reject(err);
        });
        if (post_data) {
            req.write(post_data);
        }
        // IMPORTANT
        req.end();
    });
}

export function read_remote_json(url: string): Promise<object> {
    return http_request(url, null);
}

export let context: Context = {
    error: error,
    read_local_json: read_local_json,
    read_remote_json: read_remote_json
}
