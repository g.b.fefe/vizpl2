
export function pad_left(s: string, width: number, filler: string = ' '): string {
    return (width > s.length ? filler.repeat(width - s.length) : '') + s;
}

export function pad_right(s: string, width: number, filler: string = ' '): string {
    return s + (width > s.length ? filler.repeat(width - s.length) : '');
}
