import * as AnsiColors from './ansicolors.js';
import * as Opt from './option.js';
import { Option } from './option.js';

export let block = '█';

function ocolor(s: string, color: Option<AnsiColors.code>) {
    return Opt.fold(color, (color) => AnsiColors.color(s, color), s);
}

type bar = [string, number];

export function to_string(b: bar): string {
    return b[0];
}

function bar_map_string(b: bar, fn: (s: string) => string): bar {
    let [s1, n1] = b;
    return [fn(s1), n1];
}

export function length(b: bar): number {
    return b[1];
}

function bar_add(b1: bar, b2: bar): bar {
    let [s1, n1] = b1;
    let [s2, n2] = b2;
    return [s1 + s2, n1 + n2];
}

function bar_repeat(ch: string, n: number): bar {
    return [ch.repeat(n), n];
}

function partial_bar(resolution: number, length: number, color: Option<AnsiColors.code>): bar {
    if (isNaN(length)) {
        return ['', 0]
    };

    let partial_resolution = 8;
    if (length == 0) {
        return ['', 0];
    } else if (length > resolution) {
        return ['X', 1];
    } else {
        let parts = Math.ceil(length / resolution * partial_resolution);
        let pbar = String.fromCodePoint(block.codePointAt(0) + (partial_resolution - parts));
        return [ocolor(pbar, color), 1];
    }
}

export function bar(resolution: number, start: number, length: number, color: Option<AnsiColors.code> = Opt.none()): bar {
    let bar: bar = ['', 0];

    if (isNaN(length)) {
        return bar;
    };

    // // |1___|2___|3___|4___|
    // // |    | ###|####|##  |

    let empty = ' ';
    bar = bar_add(bar, bar_repeat(empty, Math.floor(start / resolution)));
    // bar += empty.repeat(Math.floor(start / resolution));

    if (start + length < start - (start % resolution) + resolution) {
        // the bar does not fill a full block
        if (start % resolution < resolution / 2) {
            bar = bar_add(bar, partial_bar(resolution, length, color));
        } else {
            bar = bar_add(bar, bar_map_string(partial_bar(resolution, resolution - length, color), AnsiColors.inverse))
        }
    } else {
        if (start % resolution != 0) {
            let partial_start = start % resolution;
            bar = bar_add(bar, bar_map_string(partial_bar(resolution, partial_start, color), AnsiColors.inverse));
            length -= (resolution - partial_start);
        }

        // 3
        bar = bar_add(bar, bar_map_string(bar_repeat(block, Math.floor(length / resolution)), (s) => ocolor(s, color)));

        // 4
        bar = bar_add(bar, partial_bar(resolution, length % resolution, color));
    }

    return bar;
}
