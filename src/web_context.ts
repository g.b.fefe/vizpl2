import { Context } from "./context.js";

function error(msg: string): void {
    console.error(msg);
}

async function read_local_json(file: string): Promise<object> {
    return fetch(file).then(res => res.json())
}

async function read_remote_json(url: string): Promise<object> {
    return fetch(url).then(res => res.json())
}

export let context: Context = {
    error: error,
    read_local_json: read_local_json,
    read_remote_json: read_remote_json,
}
