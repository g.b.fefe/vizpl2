
// import { AsciiTable3, AlignmentEnum } from 'ascii-table3';
import * as AT from 'ascii-table3';

import { MergedDataset, MergedJob, sort_merged_dataset } from "./pipelines.js";

import { Stats, min, max } from "./statistics.js";

import { Config } from "./datafetcher.js";

import * as Opt from "./option.js";

import * as formatting from "./formatting.js";

import * as AsciiGraph from "./asciigraph.js";

import * as AnsiColors from "./ansicolors.js";

import { pad_right } from "./string.js";

export enum output_type {
    Table,
    Graph
}

// enum tabulation_sort { ByStage, ByStageAndDuration }

export function output(config: Config, baseline: MergedDataset, others: MergedDataset[]) {
    config.outputs.forEach((output) => {
        if (output === output_type.Table) {
            tabulate_results(config, baseline, others);
        } else if (output === output_type.Graph) {
            graph_results(config, baseline, others);
        }
    });
}

function job_to_row_label(job: MergedJob): string {
    let name: string;
    if (job.stage == "_all") {
        name = "[pipeline]";
    } else if (job.stage == "_stages") {
        name = `> [${job.name}]`;
    } else {
        name = `  - ${job.name}`;
    }
    return name;
}

function row_baseline(jobs: MergedJob[]): [number, MergedJob] {
    // select the first defined job in the row as the baseline
    for (let [job_idx, job] of jobs.entries()) {
        if (job != undefined) {
            return [job_idx, job];
        }
    }

}

function apply_job_filter(job_filter: Opt.Option<RegExp>, jobs_matrix: MergedJob[][]): MergedJob[][] {
    return Opt.fold(
        job_filter,
        job_filter => jobs_matrix.filter(
            jobs => {
                let [_, baseline_job] = row_baseline(jobs);
                return !!baseline_job.name.match(job_filter);
            }
        ),
        jobs_matrix
    );
}

function string_len_max(ss: string[]): number {
    return max(ss.map((s) => s.length));
}

let next_color: number = 0;
let color_of_stage: { [index: string]: AnsiColors.code } = {};
function get_color_of_stage(job: MergedJob): AnsiColors.code {
    let stage: string;
    if (job.stage == "_all") {
        stage = "pipeline";
    } else if (job.stage == "_stages") {
        stage = job.name;
    } else {
        stage = job.stage;
    }

    let graph_palette: AnsiColors.code[] = [
        AnsiColors.Codes.blue,
        AnsiColors.Codes.white,
        AnsiColors.Codes.cyan,
        AnsiColors.Codes.gray,
        AnsiColors.Codes.magenta,
        AnsiColors.Codes.green,
        AnsiColors.Codes.red,
        // used for queued
        // AnsiColors.Codes.yellow,
    ];

    if (!(stage in color_of_stage)) {
        color_of_stage[stage] = graph_palette[next_color];
        next_color = (next_color + 1 % graph_palette.length);
    }
    return color_of_stage[stage];
}


export function graph_results(config: Config, baseline: MergedDataset, others: MergedDataset[]) {

    let jobs_matrix = sort_merged_dataset(baseline, others);
    jobs_matrix = apply_job_filter(config.job_filter, jobs_matrix);

    let max_width = process.stdout.columns;

    let max_label_width = string_len_max(
        ([baseline].concat(others)).map((ds) => ds.label).concat(["Dataset"])
    );

    let max_job_name_width = string_len_max(
        jobs_matrix.flat().map((job) => job ? job_to_row_label(job) : '').concat(["Job"])
    );

    let bar_max = max_width
        - (1  // lhs
            + max_label_width
            + 3 // padding
            + max_job_name_width
            + 3 // padding
            + 12 // times
            + 10); // rhs
    let job_times =
        jobs_matrix.flat().map((job) => {
            let finish_time =
                job.started_offset_stats.mean +
                job.duration_stats.mean;
            return isNaN(finish_time) ? 0 : finish_time;
        });
    let max_finish_time = max(
        job_times
    );

    let resres = 5;
    let resolution = resres * Math.ceil(max_finish_time / bar_max / resres);

    let queued_color = AnsiColors.Codes.yellow;
    let block = AsciiGraph.block;

    let headings: string[] = [
        pad_right("Dataset", max_label_width),
        pad_right("Job", max_job_name_width),
        `Time (${block} = ${resolution}s, ${AnsiColors.color(block, queued_color)} = time queued)`
    ];

    let lines: string[] = [
        '-'.repeat(max_label_width),
        '-'.repeat(max_job_name_width),
        '------------------------------'
    ];

    let rows: string[][] =
        jobs_matrix.flatMap((jobs) => {
            let rows: string[][] = [];

            let baseline_job: MergedJob;
            // let baseline_idx: number;
            for (let [_job_idx, job] of jobs.entries()) {
                if (job != undefined) {
                    baseline_job = job;
                    // baseline_idx = job_idx;
                    break;
                }
            }

            // TODO: better handling of missing jobs
            let nan =
                isNaN(baseline_job.started_offset_stats.mean) ||
                isNaN(baseline_job.duration_stats.mean);

            let bar = '';
            if (!nan) {
                let color = get_color_of_stage(baseline_job);
                let started_at = Math.floor(baseline_job.started_offset_stats.mean);
                let queued_duration = Math.floor(baseline_job.queued_duration_stats.mean);
                let duration = Math.floor(baseline_job.duration_stats.mean);

                let duration_bar = '';
                let queued_duration_bar = '';
                if (!isNaN(queued_duration)) {
                    let queue_started_at = max([started_at - queued_duration, 0]);

                    // add and offset to make queue time line up to
                    // the right border or a column, job time time to
                    // the left
                    let lineup_offset = resolution - ((queue_started_at + queued_duration) % resolution);
                    queued_duration_bar = AsciiGraph.bar(resolution, lineup_offset + queue_started_at, queued_duration, Opt.some(queued_color))[0];
                    duration_bar = AsciiGraph.bar(
                        resolution,
                        0, // TODO: hack
                        duration,
                        Opt.some(color)
                    )[0];
                } else {
                    duration_bar = AsciiGraph.bar(
                        resolution,
                        started_at,
                        duration,
                        Opt.some(color)
                    )[0];
                }

                bar += queued_duration_bar
                    + duration_bar
                    + '   (' + formatting.humanize_time(queued_duration) + ', ' + formatting.humanize_time(duration) + ')';
            }

            // TODO: add other jobs
            rows.push([
                pad_right(baseline.label, max_label_width),
                pad_right(job_to_row_label(baseline_job), max_job_name_width),
                '▏' + bar
            ])

            return rows;

        });

    let table: string[] = [headings, lines].concat(rows).map((r) => r.join(' '));

    console.log(table.join("\n"));
}


export function tabulate_results(config: Config, baseline: MergedDataset, others: MergedDataset[]) {
    // present results thus:

    // let _config = {
    //     // sort: tabulation_sort.ByStage,
    //     humanize_durations: true,
    //     compare_baseline: true,
    //     show_stats: true,
    //     show_success: true,
    // };

    let jobs_matrix = sort_merged_dataset(baseline, others);
    jobs_matrix = apply_job_filter(config.job_filter, jobs_matrix);


    let all_stats = ["mean", "median", "stddev", "min", "max"];
    let stats_headings = (measure: string, fields = all_stats) =>
        fields.map((stat) => `${measure} (${stat})`)
    function stats_values(stat: Stats, fields = all_stats): number[] {
        return fields.map((field) => stat[field])
    }

    let include_stats = ['mean'];
    let columns_per_ds = [
        {
            headings:
                [
                    stats_headings('Duration', include_stats),
                    stats_headings('Queued duration', include_stats)
                ].flat(),
            values: ((job: MergedJob) =>
                [
                    stats_values(job.duration_stats, include_stats),
                    stats_values(job.queued_duration_stats, include_stats)
                ].flat()
            )
        },
        // {headings: stats_headings('End offset', ['mean']), values: ((job: MergedJob) => stats_values(job.started_offset_stats, ['mean']).map(d2))}
        // ['Start offset', ((job: MergedJob) => job.started_offset_stats), ['mean']],
    ];


    let headings = ['Job'];
    let datasets = [baseline].concat(others);
    headings = headings.concat(
        datasets.flatMap(
            (ds) => columns_per_ds.flatMap(
                (col, i) => col.headings.map((h) =>
                    i == 0 ? `${ds.label} (n=${ds.count}): ${h}` : h))));

    let table = new AT.AsciiTable3('Pipelines times');
    table.setHeading(...headings);
    headings.slice(1).forEach((_, i) => table.setAlign(i + 2, AT.AlignmentEnum.RIGHT));
    table.addRowMatrix(jobs_matrix.map((jobs) => {
        let row: (string | number)[] = [];

        // select the first defined job in the row as the baseline
        let [baseline_idx, baseline_job] = row_baseline(jobs);

        if (!Opt.fold(config.job_filter, job_filter => !!baseline_job.name.match(job_filter), true)) {
            return [];
        }

        row.push(job_to_row_label(baseline_job));

        let baseline_values: number[][] = [];
        for (let [job_idx, job] of jobs.entries()) {
            for (let [col_idx, col] of columns_per_ds.entries()) {
                let vals: (string | number)[];
                if (job != undefined && job_idx == baseline_idx) {
                    let values_raw = col.values(job);
                    baseline_values[col_idx] = values_raw;
                    vals = values_raw.map((v) => {
                        return config.formatter(v, Opt.none());
                    });
                } else if (job != undefined) {
                    vals = col.values(job).map((v, i) => {
                        return config.formatter(v, Opt.some(baseline_values[col_idx][i]));
                    });
                } else {
                    vals = col.headings.fill("n/a");
                }
                row = row.concat(vals);
            }
        }

        // row = row.concat(jobs.flatMap((job) => {
        //     let values : (string | number)[] = columns_per_ds.flatMap((col) => {
        //         let vals : (string | number)[];
        //         if (job != undefined) {
        //             vals = col.values(job).map((v) => {
        //                 return config.formatter(v);
        //             });
        //         } else {
        //             vals = col.headings.fill("n/a");
        //         }
        //         return vals;
        //     });
        //     return values;
        // }));

        // row = row.concat(stats_values(job.duration_stats, ['mean']).map(d2))
        // row = row.concat(stats_values(job.started_offset_stats, ['mean']).map(d2))

        // let job = jobs[0];

        return row;
    }));

    console.log(table.toString());
}
