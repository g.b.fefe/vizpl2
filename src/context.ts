
export interface Context {
    error: (msg: string) => void,
    read_local_json: (file: string) => Promise<object>,
    // local_file_exists: (file: string) => boolean
    read_remote_json: (url: string) => Promise<object>
}

