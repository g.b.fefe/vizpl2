// import * as Output from "./output.js";
import * as Pipelines from "./pipelines.js";
import * as Datafetcher from "./datafetcher.js";
import * as Statistics from "./statistics.js";
import { context } from "./web_context.js";


// import { MergedDataset, MergedJob } from "./pipelines.js";

// import { Stats, stats } from "./statistics.js";


function fetch_jobs(project_id: string, pipeline_id: number, onSuccess, onFail) {
    var per_page = 200;
    var project_id = encodeURIComponent(project_id);
    var url = "https://gitlab.com/api/v4/projects/"
        + project_id
        + "/pipelines/"
        + pipeline_id
        + "/jobs?per_page="
        + per_page;

    $.getJSON(url, onSuccess).fail(onFail);
}

function plot_pipeline() {
    var project_id: string = $('[name=project-id]').val().toString();
    var pipeline_id: number = +$('[name=pipeline-id]').val();
    console.log('project_id', project_id);
    console.log('pipeline_id', pipeline_id);

    fetch_jobs(project_id, pipeline_id, function(data: Pipelines.raw_pipeline) {
        // console.log('data', data);
        let baseline = ({
            label: `Pipeline ${project_id}#${pipeline_id}`,
            pipelines: [Pipelines.annotate_pipeline(data)]
        })
        let merged_baseline = Pipelines.merge_dataset(
            context,
            baseline
        )
        graph_merged_results(merged_baseline, []);
    }, function(err) {
        console.log('Could not fetch data for project_id, pipeline_id', project_id, pipeline_id);
        console.log('err', err);
    })
    //

    return false;
}

function graph_merged_results(
    baseline: Pipelines.MergedDataset,
    others: Pipelines.MergedDataset[]
) {
    console.log("graphing")

    let jobs_matrix = Pipelines.sort_merged_dataset(baseline, others);
    let all_jobs = jobs_matrix.flat();

    console.log("sorted")

    let max_finished_at_offset_mean =
        Statistics.max(all_jobs.map((job) =>
            job.started_offset_stats.mean + job.duration_stats.mean));


    const data_count = jobs_matrix.length;
    const number_cfg = {
        count: data_count,
        min: 0,
        max: max_finished_at_offset_mean
    };

    const labels = all_jobs.map((job) => job.name)

    let palette = [
        "#E9BBB5",
        "#E7CBA9",
        "#AAD9CD",
        "#E8D595",
        "#8DA47E",
    ];
    let next_color = 0;
    let color_per_stage = {};

    let baseline_dataset = {
        label: baseline.label,
        data: jobs_matrix.map((jobs) => [
            // todo: show other jobs
            jobs[0].started_offset_stats.mean,
            jobs[0].started_offset_stats.mean + jobs[0].duration_stats.mean
        ]),
        backgroundColor: jobs_matrix.map((jobs) => {
            let job = jobs[0];
            let stage: string;
            if (job.stage == "_all") {
                stage = "pipeline";
            } else if (job.stage == "_stages") {
                stage = job.name;
            } else {
                stage = job.stage;
            }
            if (!(stage in color_per_stage)) {
                color_per_stage[stage] = palette[next_color];
                next_color = (next_color + 1 % palette.length);
            }
            return color_per_stage[stage];
        })
    }

    let data = {
        labels: labels,
        datasets: [baseline_dataset]
    };

    const config = {
        type: 'bar',
        data: data,
        options: {
            indexAxis: 'y',

            // animation: false,
            animation: {
                duration: 0
            },

            // maintainAspectRatio: false,
            responsive: false,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Pipeline'
                },
                // tooltip: {
                //     callbacks: {
                //         afterLabel: (item, object) => "foo"
                //     }
                // }
            },
            scales: {
                x: {
                    position: 'top',
                    title: {
                        display: true,
                        text: 'Time in minutes'
                    },
                    ticks: {
                        // forces step size to be 50 units
                        stepSize: 60,
                        callback: (tickValue: number, index, ticks) => (
                            tickValue / 60
                        )
                    }
                }
            }
        }
    }

    console.log("start drawing")
    // @ts-ignore
    new Chart($('#canvas')[0].getContext('2d'), config);
    console.log("done")
}

function compare_datasets(_config: Datafetcher.Config, baseline: Pipelines.Dataset, others: Pipelines.Dataset[]) {
    let baseline_merged = Pipelines.merge_dataset(context, baseline);
    let others_merged = others.map((ds) => Pipelines.merge_dataset(context, ds))
    graph_merged_results(baseline_merged, others_merged);

    // Output.tabulate_results(baseline_merged, others_merged)
    // console.log(util.inspect(merged, { showHidden: false, depth: 3, colors: true }))
}


export function vizpl() {
    console.log('vizpl');

    let params = new URLSearchParams(window.location.search);
    if (params.has('cli_args')) {
        let args = params.get('cli_args').split(' ');
        Datafetcher.parse_command_line(context, args).then(
            (args: Datafetcher.Arguments) =>
                compare_datasets(args.config, args.datasets[0], args.datasets.slice(1))
        )
    } else {
        // fetch("b__m_jobs/pipeline-385075830.json")
        //     .then(res => res.json())
        //     .then(data => console.log(data))
    }

    $('#pipeline-plot').click(plot_pipeline);
}
