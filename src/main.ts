'use strict';

import * as Output from "./output.js";
import * as Pipelines from "./pipelines.js";
import * as Datafetcher from "./datafetcher.js";
import { context } from "./cli_context.js";

export function compare_datasets(args: Datafetcher.Arguments) {
    let baseline_merged = Pipelines.merge_dataset(context, args.datasets[0]);
    let others_merged = args.datasets.slice(1).map((ds) => Pipelines.merge_dataset(context, ds))

    Output.output(args.config, baseline_merged, others_merged)
}

Datafetcher.parse_command_line(context, process.argv.slice(2)).then(
    (args: Datafetcher.Arguments) => compare_datasets(args)
)
