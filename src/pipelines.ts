'use strict';

import { Context } from "./context.js";
import * as Statistics from "./statistics.js";
import { Stats } from "./statistics.js";
import * as Vzdate from "./vzdate.js";

// import { error } from "./error.js";

interface JobRaw {
    name: string;
    duration: number | null;
    queued_duration: number | null;
    status: string;
    pipeline: {
        id: number
    },

    stage: string;
    allow_failure: boolean;
    started_at: string | null;
    finished_at: string | null;
}

export type raw_pipeline = JobRaw[]

// we preprocess raw pipelines to add some extra informatoin
interface JobAnnotations {
    // the offset of the starting date w.r.t. earlier job in pipeline
    started_at_offset: number
}

// jobs as returned by gitlab api after preprocessing
interface JobAnnotated extends JobRaw, JobAnnotations {
}

// an abstract job is either a normal job or a pseudo job (stages,
// all). it is always annotated
interface AbstractJob extends JobAnnotations {
    name: string;
    stage: string;
    duration: number;
    queued_duration: number;
    status: string;
    pipeline: {
        id: number
    }
}

export type pipeline = JobAnnotated[]

export function annotate_pipeline(raw_pl: raw_pipeline): pipeline {
    let min_start_date = Statistics.min(
        raw_pl
            .filter((raw_job) => raw_job.started_at != null)
            .map((raw_job) => Vzdate.date_str_to_epoch(raw_job.started_at))
    );

    let pl = raw_pl.map((raw_job) => {
        let started_at_offset =
            raw_job.started_at != null
                ? Vzdate.date_str_to_epoch(raw_job.started_at) - min_start_date
                : null;
        return {
            ...raw_job,
            started_at_offset: started_at_offset
        }
    })
    return pl;
}

// Merged jobs and datasets

export interface Dataset {
    label: string;
    pipelines: pipeline[];
}

interface StatusStats {
    success: number;
    failure: number;
    other: number;
    missing: number;
}

// Represents either a set of merged concrete jobs or a set of merged pseudo jobs
export interface MergedJob {
    name: string,
    started_offset_stats: Stats;
    duration_stats: Stats;
    queued_duration_stats: Stats;
    status_stats: StatusStats;
    jobs: AbstractJob[];

    // pseudo-jobs do not have the following properties
    stage: string;
}

export interface MergedDataset {
    label: string;
    // number of merged sets
    count: number;
    merged_jobs: { [index: string]: MergedJob };
    merged_stages: { [index: string]: MergedJob };
    all: MergedJob;
}

function merge_jobs(
    ctxt: Context,
    ds_length: number,
    job_name: string,
    jobs: AbstractJob[]
): MergedJob {
    if (jobs.length == 0) {
        ctxt.error("cannot merge 0 jobs");
    }

    let stage = jobs[0].stage;
    let other_stages = jobs.filter((job) => job.stage != stage)
    if (other_stages.length != 0) {
        ctxt.error("expected jobs to all contains jobs of stage ${stage}");
    }

    let durations = jobs
        .filter((job) => job.duration !== null && job.duration > 0)
        .map((job) => job.duration);

    let queued_durations = jobs
        .filter((job) => job.queued_duration !== null && job.queued_duration > 0)
        .map((job) => job.queued_duration);

    if (durations.length == 0) {
        // console.warn("Found no durations for job: " + job_name)
        // continue;
    }

    let started_offsets = jobs.map((job) => job.started_at_offset);

    return {
        name: job_name,
        stage: stage,
        started_offset_stats: Statistics.stats(started_offsets),
        duration_stats: Statistics.stats(durations),
        queued_duration_stats: Statistics.stats(queued_durations),
        status_stats: {
            success: Statistics.count(jobs, (job) => job.status == "success"),
            failure: Statistics.count(jobs, (job) => job.status == "failure"),
            other: Statistics.count(jobs, (job) => job.status != "failure" && job.status != "success"),
            missing: ds_length - jobs.length,
        },
        jobs: jobs
    };
}

function merge_jobs_hash(
    ctxt: Context,
    ds_length: number,
    jobs_hash: { [index: string]: AbstractJob[] }
): { [index: string]: MergedJob } {

    let merged_jobs: { [index: string]: MergedJob } = {}
    for (const job_name of Object.keys(jobs_hash)) {
        merged_jobs[job_name] = merge_jobs(ctxt, ds_length, job_name, jobs_hash[job_name])
    }
    return merged_jobs;
}


function status_of_jobs(jobs: JobAnnotated[]): string {
    let failed_jobs = jobs.filter((job) => job.status != "success" && !job.allow_failure)
    // if (failed_jobs.length > 0) {
    // console.warn("considering stage " + stage + " in pipeline " + jobs[0].pipeline.id + "as failure due to jobs:")
    // failed_jobs.forEach((job) => console.log(job))
    // }
    return (failed_jobs.length > 0 ? "failure" : "success");
}

export function merge_dataset(
    ctxt: Context,
    ds: Dataset): MergedDataset {
    let jobs_by_name: { [index: string]: AbstractJob[] } = {}
    ds.pipelines.forEach((pipeline) => {
        pipeline.forEach((job) => {
            if (job.name in jobs_by_name) {
                jobs_by_name[job.name].push(job)
            } else {
                jobs_by_name[job.name] = [job]
            }
        })
    });

    let merged_jobs = merge_jobs_hash(ctxt, ds.pipelines.length, jobs_by_name);

    let stages_by_name: { [index: string]: AbstractJob[] } = {}
    ds.pipelines.forEach((pipeline) => {
        let pipeline_id = pipeline[0].pipeline.id;

        let stage_names: Set<string> = new Set(pipeline.map((job) => job.stage));
        let stages = [];
        for (let stage of stage_names) {
            let jobs = pipeline.filter((job) => job.stage == stage);
            if (jobs.length == 0) {
                // console.warn("Found no jobs in stage: " + stage + ", of pipeline" + jobs[0].pipeline.id)
                continue;
            }

            let stage_status = status_of_jobs(jobs);

            let jobs_with_duration =
                jobs.filter((job) => job.duration !== null && job.duration > 0)
            let durations = jobs_with_duration.map((job) => job.duration);

            let jobs_with_queued_duration =
                jobs.filter((job) => job.queued_duration !== null && job.queued_duration > 0)
            let queued_durations = jobs_with_queued_duration.map((job) => job.queued_duration);

            if (durations.length == 0) {
                // console.warn("Found no durations for jobs in stage: " + stage + " in pipeline " + jobs[0].pipeline.id + ", skipping")
                continue;
            }

            let mean_started_at = Statistics.mean(
                jobs_with_duration.map((job) => job.started_at_offset)
            );

            let stage_job: AbstractJob = {
                name: stage,
                stage: "_stages", // TODO: hack
                started_at_offset: mean_started_at,
                duration: Statistics.max(durations),
                queued_duration: Statistics.max(queued_durations),
                status: stage_status,
                pipeline: { id: pipeline_id }
            }
            if (stage in stages_by_name) {
                stages_by_name[stage].push(stage_job);
            } else {
                stages_by_name[stage] = [stage_job];
            }
        }
        return stages;
    });
    let merged_stages = merge_jobs_hash(ctxt, ds.pipelines.length, stages_by_name);

    let all_jobs: AbstractJob[] =
        ds.pipelines.map((pipeline) => {
            let pipeline_id = pipeline[0].pipeline.id;

            let finished_jobs = pipeline.filter((job) => job.finished_at != null)
            let started_at_offset = Statistics.min(finished_jobs.map((job) => job.started_at_offset));

            let start_time = Statistics.min(finished_jobs.map((job) => Vzdate.date_str_to_epoch(job.started_at)));
            let end_time = Statistics.max(finished_jobs.map((job) => Vzdate.date_str_to_epoch(job.finished_at)));
            let duration = end_time - start_time;

            let all_status = status_of_jobs(pipeline);
            return {
                name: "all",
                stage: "_all",
                started_at_offset: started_at_offset,
                duration: duration,
                // TODO: does this make any sense?
                queued_duration: 0,
                status: all_status,
                pipeline: { id: pipeline_id }
            }
        });
    let merged_all = merge_jobs(ctxt, ds.pipelines.length, "all", all_jobs);

    return {
        label: ds.label,
        count: ds.pipelines.length,
        merged_jobs: merged_jobs,
        merged_stages: merged_stages,
        all: merged_all
    }
}

function sort_jobs_of_stage(stage_name: string, baseline: MergedDataset, others: MergedDataset[]): (MergedJob | undefined)[][] {
    let baseline_stage_jobs = Object
        .values(baseline.merged_jobs)
        .filter((job) => job.stage == stage_name);
    baseline_stage_jobs.sort((j1, j2) => j2.duration_stats.mean - j1.duration_stats.mean)

    // for each job, find corresponding in others and add it or null
    let stage_job_rows: (MergedJob | undefined)[][] =
        baseline_stage_jobs.map((baseline_stage_job) =>
            [baseline_stage_job].concat(others.map((ds) => {
                let job = ds.merged_jobs[baseline_stage_job.name];
                if (job != undefined && job.stage == stage_name) {
                    delete ds.merged_jobs[baseline_stage_job.name];
                    return job;
                } else {
                    return undefined;
                }
            })));

    baseline_stage_jobs.forEach((job) => delete baseline.merged_jobs[job.name]);

    if (others.length != 0) {
        let others_stage_jobs_rows = sort_jobs_of_stage(
            stage_name, others[0], others.slice(1)
        );
        others_stage_jobs_rows.forEach((row) => row.unshift(undefined));
        stage_job_rows = stage_job_rows.concat(others_stage_jobs_rows)
    }

    return stage_job_rows;
}

export function sort_merged_dataset(baseline: MergedDataset, others: MergedDataset[]): (MergedJob | undefined)[][] {
    let job_matrix: (MergedJob | undefined)[][] = [];

    job_matrix.push([baseline.all].concat(others.map((ds) => ds.all)));

    let stages_sorted: MergedJob[] = Object.values(baseline.merged_stages);
    stages_sorted = stages_sorted.sort(
        (s1, s2) => s1.started_offset_stats.median - s2.started_offset_stats.median
    )

    for (let stage_job of stages_sorted) {
        let stage_name = stage_job.name;
        job_matrix.push([stage_job].concat(others.map((ds) => ds.merged_stages[stage_name])));
        job_matrix = job_matrix.concat(sort_jobs_of_stage(stage_name, baseline, others));
    }

    // TODO
    others.forEach((ds) => Object.values(ds.merged_jobs).forEach((job) => console.warn(`TODO: stage ${job.stage}: orphan job ${job.name}`)));

    let row_lengths = job_matrix.map((row) => row.length);
    if ((new Set(row_lengths)).size != 1) {
        console.error("Row lengths are funky", row_lengths)
    }

    return job_matrix;
}

