#!/bin/sh

url_encode() {
    jq -rn --arg x "$*" '$x|@uri' ;
}

firefox "built/index.html?cli_args=$(url_encode $@)"
