# vizpl2

Tool for visualizing GitLab CI pipelines.

## Reminder on how to install node and so forth

Install nvm and restart terminal:

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

Install the latest LTS version of node:

```sh
nvm install v16.14.0
```

Then go on to the next section.

## Installation & Usage

`vizpl2` can either be installed locally using `npm` or run through Docker.

### Installation with `npm`

First run

```sh
npm install
```

then build the application with `npm run build`.

To use the locally installed version of `vizpl2`, run commands using
`cli.sh`, e.g.:

```
$ ./cli.sh --graph 'nomadic-labs/tezos#407818216'
```

### Usage with Docker

When using Docker, no building is needed. The provided convenience
script `docker.sh` builds a Docker image and then runs it
directly. This script takes the same arguments as `cli.sh`, e.g.:

```
$ ./docker.sh --graph 'nomadic-labs/tezos#407818216'
```

### Building, testing

Use the package scripts defined in `package.json`:

 - `npm run build`: to compile typescript to javascript
 - `npm run start`: to compile typescript to javascript continiously in the background
 - `npm run test`: to run the test suite

## Examples

Pipelines are referred to as
`namespace/project#pipeline_id`. `namespace` and `project` both
default to `tezos`. So `tezos/tezos#1234`, `tezos#1234`, `#1234` all
refer to the same pipeline.


```
$ ./cli.sh --graph 'nomadic-labs/tezos#407818216'
                                                          Pipeline graph                                                           
           Dataset                               Job                                          Time (█ = 55s)                       
 nomadic-labs/tezos#407818216   all                                     ▏███████████████████████████▊   (25m22.42s) 
 nomadic-labs/tezos#407818216   sanity                                  ▏█▌   (1m20.04s)                            
 nomadic-labs/tezos#407818216   sanity_ci                               ▏█▌   (1m20.04s)                            
 nomadic-labs/tezos#407818216   docker-hadolint                         ▏▎   (12.02s)                               
 nomadic-labs/tezos#407818216   build                                   ▏█████████   (8m12.96s)                     
 nomadic-labs/tezos#407818216   build_x86_64                            ▏█████████   (8m12.96s)                     
 nomadic-labs/tezos#407818216   documentation:build                     ▏████████▊   (7m57.4s)                      
 nomadic-labs/tezos#407818216   build_arm64 (x)                         ▏                                                          
 nomadic-labs/tezos#407818216   test                                    ▏        ██████████████████▌   (16m50.84s)  
 nomadic-labs/tezos#407818216   unit:011_PtHangz2                       ▏         ██████████████████▌   (16m50.84s) 
 nomadic-labs/tezos#407818216   unit:alpha                              ▏         ████████████████▏   (14m45.01s)   
 nomadic-labs/tezos#407818216   unit:non-proto-x86_64                   ▏         ██████████████▉   (13m32.67s)     
 nomadic-labs/tezos#407818216   integration:pytest 1/25                 ▏         ██████████████▍   (13m9.21s)      
 nomadic-labs/tezos#407818216   unit:010_PtGRANAD                       ▏         ██████████████▍   (13m6.71s)      
 nomadic-labs/tezos#407818216   integration:pytest 12/25                ▏         ██████████████▏   (12m52.35s)     
 nomadic-labs/tezos#407818216   integration:pytest 2/25                 ▏         ██████████████   (12m46.83s)      
 nomadic-labs/tezos#407818216   integration:pytest 11/25                ▏         █████████████▉   (12m36.71s)      
 nomadic-labs/tezos#407818216   integration:pytest 3/25                 ▏         █████████████▊   (12m34.31s)      
 nomadic-labs/tezos#407818216   integration:pytest 4/25                 ▏         █████████████▌   (12m18.59s)      
 nomadic-labs/tezos#407818216   tezt:1                                  ▏         ███████████▋   (10m38.87s)        
 nomadic-labs/tezos#407818216   tezt:3                                  ▏         ███████████▋   (10m34.57s)        
 nomadic-labs/tezos#407818216   tezt:2                                  ▏         ███████████▌   (10m31.36s)        
 nomadic-labs/tezos#407818216   integration:pytest 25/25                ▏         ██████████▏   (9m10.93s)          
 nomadic-labs/tezos#407818216   integration:pytest 8/25                 ▏         █████████▉   (8m57.54s)           
 nomadic-labs/tezos#407818216   integration:pytest 14/25                ▏         █████████▊   (8m54.37s)           
 nomadic-labs/tezos#407818216   integration:pytest 7/25                 ▏         █████████▋   (8m47.94s)           
 nomadic-labs/tezos#407818216   integration:pytest 20/25                ▏         █████████▌   (8m36.96s)           
 nomadic-labs/tezos#407818216   integration:pytest 6/25                 ▏         █████████▏   (8m15.94s)           
 nomadic-labs/tezos#407818216   integration:pytest 24/25                ▏         █████████   (8m12.43s)            
 nomadic-labs/tezos#407818216   integration:pytest 15/25                ▏         ████████▉   (8m5.27s)             
```

## Tables

```
$ ./cli.sh --humanize --table 'nomadic-labs/tezos#407818216'
+-------------------------------------------------------------------------------------------------+
|                                         Pipelines times                                         |
+-------------------------------------------+-----------------------------------------------------+
|                    Job                    | nomadic-labs/tezos#407818216 (n=1): Duration (mean) |
+-------------------------------------------+-----------------------------------------------------+
| [pipeline]                                |                                           25m22.42s |
| > [sanity]                                |                                            1m20.04s |
|   - sanity_ci                             |                                            1m20.04s |
|   - docker-hadolint                       |                                              12.02s |
| > [build]                                 |                                            8m12.96s |
|   - build_x86_64                          |                                            8m12.96s |
|   - documentation:build                   |                                             7m57.4s |
|   - build_arm64                           |                                                 NaN |
| > [test]                                  |                                           16m50.84s |
|   - unit:011_PtHangz2                     |                                           16m50.84s |
|   - unit:alpha                            |                                           14m45.01s |
|   - unit:non-proto-x86_64                 |                                           13m32.67s |
|   - integration:pytest 1/25               |                                            13m9.21s |
|   - unit:010_PtGRANAD                     |                                            13m6.71s |
|   - integration:pytest 12/25              |                                           12m52.35s |
|   - integration:pytest 2/25               |                                           12m46.83s |
|   - integration:pytest 11/25              |                                           12m36.71s |
|   - integration:pytest 3/25               |                                           12m34.31s |
|   - integration:pytest 4/25               |                                           12m18.59s |
|   - tezt:1                                |                                           10m38.87s |
|   - tezt:3                                |                                           10m34.57s |
```

### Multiple data sets

You can provide multiple pipeline references on the command line:


```
$ ./cli.sh --humanize --table 'tezos/tezos#407662764' 'tezos/tezos#407653513'

+-----------------------------------------------------------------------------------------------------------------------------------------+
|                                                             Pipelines times                                                             |
+-------------------------------------------+----------------------------------------------+----------------------------------------------+
|                    Job                    | tezos/tezos#407662764 (n=1): Duration (mean) | tezos/tezos#407653513 (n=1): Duration (mean) |
+-------------------------------------------+----------------------------------------------+----------------------------------------------+
| [pipeline]                                |                                    31m36.33s |                         25m46.13s (- %22.65) |
| > [sanity]                                |                                     1m18.58s |                           1m16.31s (- %2.97) |
|   - sanity_ci                             |                                     1m18.58s |                           1m16.31s (- %2.97) |
|   - docker-hadolint                       |                                       11.29s |                            12.85s (+ %13.82) |
| > [build]                                 |                                     7m59.62s |                           8m35.37s (+ %7.45) |
|   - documentation:build                   |                                     7m59.62s |                            8m6.92s (+ %1.52) |
|   - build_x86_64                          |                                     6m21.56s |                          8m35.37s (+ %35.07) |
|   - build_arm64                           |                                          NaN |                                 NaN (- %NaN) |
| > [test]                                  |                                    18m17.89s |                           16m57.54s (- %7.9) |
|   - unit:011_PtHangz2                     |                                    18m17.89s |                           16m57.54s (- %7.9) |
|   - unit:alpha                            |                                    14m49.65s |                         11m32.14s (- %28.54) |
|   - integration:pytest 10/25              |                                     14m36.2s |                          13m42.53s (- %6.53) |
|   - integration:pytest 4/25               |                                    13m51.42s |                          12m7.85s (- %14.23) |
```

### Averaging several pipelines in one data sets

You can average pipelines by finishing a sequence of pipeline reference with the `--label <label-without-spaces`:

```
$ ./cli.sh --humanize --table \
    'tezos/tezos#407662764' 'tezos/tezos#407653513' --label 'Dataset1' \
    'tezos/tezos#407646210' 'tezos/tezos#407608659' --label 'Dataset2'
TODO: stage manual: orphan job publish:docker_manual
TODO: stage manual: orphan job documentation:linkcheck
TODO: stage manual: orphan job documentation:build_all
TODO: stage test_coverage: orphan job test_coverage: [test-tezt-coverage]
TODO: stage test_coverage: orphan job test_coverage: [test-python-alpha]
TODO: stage test_coverage: orphan job test_coverage: [test-unit]
TODO: stage doc: orphan job compile_sources_buster
TODO: stage doc: orphan job install_opam_focal
TODO: stage doc: orphan job install_opam_bionic
TODO: stage doc: orphan job install_bin_fedora_34
TODO: stage doc: orphan job install_bin_fedora_33
TODO: stage doc: orphan job install_bin_focal
TODO: stage doc: orphan job install_bin_bionic
+---------------------------------------------------------------------------------------------------------------+
|                                                Pipelines times                                                |
+-------------------------------------------+---------------------------------+---------------------------------+
|                    Job                    | Dataset1 (n=2): Duration (mean) | Dataset2 (n=2): Duration (mean) |
+-------------------------------------------+---------------------------------+---------------------------------+
| [pipeline]                                |                       28m41.23s |            25m36.63s (- %12.01) |
| > [build]                                 |                        8m17.49s |               7m43.21s (- %7.4) |
|   - documentation:build                   |                         8m3.27s |              6m7.93s (- %31.35) |
|   - build_x86_64                          |                        7m28.47s |              7m43.21s (+ %3.29) |
|   - build_arm64                           |                             NaN |                    NaN (- %NaN) |
| > [sanity]                                |                        1m17.44s |               44.65s (- %73.46) |
|   - sanity_ci                             |                        1m17.44s |               44.33s (- %74.69) |
|   - docker-hadolint                       |                          12.07s |                9.12s (- %32.39) |
| > [test]                                  |                       17m37.72s |             16m42.96s (- %5.46) |
|   - unit:011_PtHangz2                     |                       17m37.72s |             16m42.96s (- %5.46) |
|   - integration:pytest 10/25              |                        14m9.37s |              14m44.5s (+ %4.14) |
|   - unit:010_PtGRANAD                     |                       13m13.01s |             14m15.24s (+ %7.85) |
|   - unit:alpha                            |                       13m10.89s |             12m40.22s (- %4.03) |
|   - integration:pytest 4/25               |                       12m59.64s |             13m32.75s (+ %4.25) |
|   - integration:pytest 2/25               |                       12m50.53s |            14m38.98s (+ %14.08) |
|   - integration:pytest 1/25               |                       12m50.51s |             13m32.37s (+ %5.43) |
```

## How to read the results

How to read the results:

  - the mean duration of the pipeline (job name written as
    `[pipeline]`) is the mean end time of the
    job finishing last.

  - the mean duration time of stages (job name written as `> [ stage
    name ]`) is the average duration of slowest job of the stage in
    each pipeline of the data set:
    `average(forall ds in datasets: max(forall job in ds.stage: job.duration))`.
    In other words, it is not the `mean end time of the stage - mean start time`
    of the stage.

This means that the mean duration of pipelines depends on how runners
are scheduled to jobs. On the other hand, this is not true for the
mean duration of stages, and therefore comparisons made there
allowing us to disregard variations in runner availability.

Ideally, we should calculate the critical path for a pipeline, and
them sum the mean duration of each step on the critical path, but the
tools does not yet handle this.

